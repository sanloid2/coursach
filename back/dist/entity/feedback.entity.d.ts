export declare class Feedback {
    id: number;
    title: string;
    description: string;
    upvote: number;
    comments: number;
}
