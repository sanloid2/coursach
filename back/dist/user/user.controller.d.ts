import { CreateUserDto } from './user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    getUser(): Promise<User[]>;
    create(user: CreateUserDto): Promise<any>;
    delete(id: number): Promise<void>;
}
