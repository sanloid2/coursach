import { Repository } from 'typeorm';
import { CreateUserDto } from './user.dto';
import { User } from './user.entity';
export declare class UserService {
    private userRepo;
    constructor(userRepo: Repository<User>);
    getAllUser(): Promise<User[]>;
    createUser(dto: CreateUserDto): Promise<{
        name: string;
    }>;
    deleteUser(id: any): Promise<void>;
    findUserByName(name: string): Promise<User>;
}
