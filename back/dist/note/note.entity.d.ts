export declare class Note {
    id: number;
    title: string;
    body: string;
}
