import { Repository } from 'typeorm';
import { Note } from './note.entity';
export declare class NoteService {
    private noteRepo;
    constructor(noteRepo: Repository<Note>);
    findAll(): Promise<Note[]>;
    create(newNote: Note): void;
    delete(id: any): void;
    update(note: Note): void;
}
