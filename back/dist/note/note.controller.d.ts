import { Note } from './note.entity';
import { NoteService } from './note.service';
export declare class NoteController {
    private readonly noteService;
    constructor(noteService: NoteService);
    getAll(): Promise<Note[]>;
    addOne(newNote: Note): Promise<void>;
    delete(noteId: number): Promise<void>;
    update(note: Note): Promise<void>;
}
