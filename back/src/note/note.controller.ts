import { Body, Controller, Delete, Get, Post, UseGuards } from '@nestjs/common';
import { Note } from './note.entity';
import { NoteService } from './note.service';

@Controller('note')
export class NoteController {
  constructor(private readonly noteService: NoteService) {}

  @Get('all')
  async getAll(): Promise<Note[]> {
    return this.noteService.findAll();
  }

  @Post('add')
  async addOne(@Body() newNote: Note) {
    this.noteService.create(newNote);
  }

  @Delete('del')
  async delete(@Body() noteId: number) {
    this.noteService.delete(noteId);
  }

  @Post('update')
  async update(@Body() note: Note) {
    this.noteService.update(note);
  }
}
