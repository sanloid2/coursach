import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { IPost } from "../model/IPost";
import { noteAPI } from "../services/userService";
import { postsSlice } from "../store/reducers/postSclie";
import s from "./../styles/post.module.css";

export default function Posts() {
  const [delPost, delRed] = noteAPI.useDeletePostMutation();
  const { data: posts, isSuccess, refetch } = noteAPI.useFetchAllPostsQuery("");

  return (
    <div>
      <button onClick={refetch}> reload </button>
      {posts ? (
        posts.map((post, id) => (
          <div key={post.id} className={s.post}>
            <h1>{post.title}</h1>
            <h2>{post.body}</h2>
            <button onClick={(e) => {delPost(post.id)}}> delete </button>
          </div>
        ))
      ) : (
        <div>posts is loading...</div>
      )}
    </div>
  );
}
