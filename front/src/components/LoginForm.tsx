import s from "./../styles/LoginForm.module.css";
import React, { useState } from "react";
import { authAPI } from "../services/authService";
import { useAppDispatch } from "../hooks/redux";
import { authSlice } from "../store/reducers/authSlice";

export default function LoginForm() {
  const [pas, setPas] = useState("");
  const [name, setName] = useState("");
  const dispatch = useAppDispatch();

  let [flag, setFla] = useState(false);
  const [login, { isLoading }] = authAPI.useLoginMutation();

  async function signin() {
    try {
      const user = await login({ username: name, password: pas }).unwrap();
      dispatch(authSlice.actions.login(user));
    } catch (e) {
      setFla(true);
    }
  }

  return (
    <div className={s.logLog}>
      <div className={s.logForm}>
        <h1>Sign in</h1>
        Username
        <input
          value={name}
          onChange={(e) => setName(e.target.value)}
          type="text"
          placeholder="username"
        />
        Password
        <input
          value={pas}
          onChange={(e) => setPas(e.target.value)}
          type="password"
          placeholder="password"
        />
        {flag ? <> wrong password or username </> : <></>}
        <button onClick={signin}> sign in </button>
      </div>
      <div className={s.logForm}>
        Don't have an account?
        <button> Sign up</button>
      </div>
    </div>
  );
}
