import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { noteAPI } from "../services/userService";
import { postsSlice } from "../store/reducers/postSclie";
import s from "./../styles/PostForm.module.css";

export default function PostForm() {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [addNew, res] = noteAPI.useAddNewPostMutation();

  const addNewPost = async (e: any) => {
    const pst = { title, body };
    await addNew(pst);
  };
  return (
    <div className={s.postForm}>
      <input
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="title..."
      ></input>
      <input
        value={body}
        onChange={(e) => setBody(e.target.value)}
        placeholder="body..."
      ></input>
      <button onClick={addNewPost}> add new post </button>
    </div>
  );
}
