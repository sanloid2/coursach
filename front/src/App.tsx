import React from "react";
import Posts from "./components/Posts";
import PostForm from "./components/PostForm";
import LoginForm from "./components/LoginForm";
import { useAppSelector } from "./hooks/redux";

function App() {
  let isLoged = useAppSelector(state => state.autchReducer.isLoged);
  return (
    <div>
      {isLoged ? (
        <>
          <PostForm />
          <Posts />
        </>
      ) : (
        <LoginForm />
      )}
    </div>
  );
}

export default App;
