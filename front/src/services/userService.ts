import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IPost } from "../model/IPost";

export const noteAPI = createApi({
  reducerPath: "userApi",
  baseQuery: fetchBaseQuery({ baseUrl: "http://localhost:5000/note" }),
  tagTypes: ["Post"],
  endpoints: (build) => ({
    fetchAllPosts: build.query<IPost[], string>({
      query: () => ({
        url: "all",
      }),
      providesTags: ["Post"],
    }),
    fetchAllPostsByUserId : build.query<IPost[], string>({
        query: (body) => ({
            url : "allById",
            body : body,
        }),
    }),
    addNewPost: build.mutation<void, IPost>({
      query: (body) => ({
        url: "add",
        method: "POST",
        body,
      }),
      invalidatesTags: ["Post"],
    }),
    deletePost: build.mutation<void, number | undefined>({
      query: (id) => ({
        url: "del",
        method: "DELETE",
        body: { id: id },
      }),
      invalidatesTags: ["Post"],
    }),
  }),
});
