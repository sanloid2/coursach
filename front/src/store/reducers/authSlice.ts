import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IPost } from "../../model/IPost";

interface AuthState {
  isLoged : boolean;
  token : string | null;
}

const initialState: AuthState = {
  isLoged: false,
  token : "",
};

export const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    login(state, action) {
      state.isLoged = true;
      state.token = action.payload.token; 
    },
  },
});

export default authSlice.reducer;
